﻿using System.Text.RegularExpressions;

namespace RegexLab
{
    public class RegexChecker
    {
        public string Pattern { get; set; }

        public bool IsValid(string str)
        {
            return Regex.Match(str, Pattern).Success;
        }

        public int CountValid(string[] strings)
        {
            int result = 0;
            foreach (var str in strings)
            {
                if (IsValid(str))
                    ++result;
            }

            return result;
        }
    }
}