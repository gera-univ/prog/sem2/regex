﻿using System;
using System.IO;

namespace RegexLab
{
    class Program
    {
        static void Main(string[] args)
        {   
            string inputFileName = "input.txt";
            string outputFileName = "output.txt";
            if (args.Length == 2)
            {
                inputFileName = args[0];
                outputFileName = args[1];
            } else if (args.Length == 0)
            {
                Console.WriteLine("Enter the input file name: ");
                inputFileName = Console.ReadLine();
                Console.WriteLine("Enter the output file name: ");
                outputFileName = Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Incorrect arguments, the default values will be used.");
            }

            try
            {
                CheckVehicleNumbers(inputFileName, outputFileName);
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine($"File {ex.FileName} is not found!");
            }
            catch (ArgumentException)
            {
                Console.WriteLine($"Empty file name is not legal.");
            }
        }

        private static void CheckVehicleNumbers(string inputFileName, string outputFileName)
        {
            var chkGermanVehicleNumbers = new RegexChecker { Pattern = @"^[A-Z]{1,3}\s(?!(SS|SD))[A-Z]{1,2}[1-9][0-9]{3}$" };
            var numberStrings = File.ReadAllLines(inputFileName);
            var nValid = chkGermanVehicleNumbers.CountValid(numberStrings);
            Console.WriteLine($"{nValid} numbers are valid.");
            File.WriteAllText(outputFileName, $"{nValid}");
        }
    }
}